<?php
/**
*   Lotto Draw Machine - lotto with powerballs
*
*   5 to 7 main balls are drawn from a Main Set of 40 to 49 balls
*   0 to 3 powerballs are drawn from a Powerball Set of 5 to 49 balls
*   Last 10 draws are shown, along with their draw times
*   Last 100 winning combinations and their draw times are saved to disk
*
*   Play button:
*   The correct number of balls are chosen from the Main Set and the Powerball Set
*   The combination is displayed, showing the main balls and powerball(s), if any
*
*   Export All button:
*   All the previous drawn combinations from the persistence store are presented to the user as a CSV file
*
*   Requires PHP >= 5.2.1
*/

require_once('lotto.class.php');

$lotto = new Lotto();

if ($_POST && isset($_POST['export'])) $lotto->export();

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Lotto Draw Machine</title>
    <style>
        #draw { margin-top: 2em }
    </style>
</head>
<body>

<h1>Lotto Draw Machine</h1>

<form method="post" action="">
    <input type="submit" name="play" value="Play" />
    <input type="submit" name="export" value="Export All" />
</form>

<?php
$savedDraws = $lotto->getDraws(LAST_DRAWS);

if ($_POST && isset($_POST['play'])) $newDraw = $lotto->draw();

if ($newDraw) {
    if ($newDraw['error']) {
        echo $newDraw['error'] . '<br>';
    } else {
        echo '<div id="draw">Time: ' . $newDraw['time'] . '<br>';
        echo 'Main: ' . $newDraw['main'] . '<br>';
        echo 'Power: ';
        if ($newDraw['power'] != '') {
            echo $newDraw['power'];
        } else {
            echo 'none';
        }
        echo '<br></div>';
    }
}

if ($savedDraws) {
    echo "<h2>Last 10 Draws</h2>\n<table border=\"1\" cellpadding=\"3\">\n";
    echo "<tr><th>Time</th><th>Main Set</th><th>Power Set</th></tr>\n";
    foreach ($savedDraws as $draw) {
        $aDraw = explode(',', $draw);
        echo '<tr><td>' . $aDraw[0] . '</td><td>' . $aDraw[1];
        echo '</td><td>' . $aDraw[2] . "</td></tr>\n";
    }
    echo "</table>\n";
}
?>

</body>
</html>

