This is an assessment I did for a PHP job application in early 2017.

I'm sure there are algorithms widely accepted by the gaming industry for generating random sequences, but I did no research on that. This exercise was about my own quick (and maybe dirty) approach using only my own code. Please also bear in mind that I wouldn't be writing HTML / PHP spaghetti code in a normal situation.

By the way, this assessment did get me a second interview, so apparently the standard was at least passable.