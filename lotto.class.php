<?php

class Lotto
{

    /* Configuration */

    // How many last draws to display
    const LAST_DRAWS = 10;

    // How many draws to save
    const SAVED_DRAWS = 100;

    // Number of balls in main set
    const BALLS_MAIN_MIN = 40;
    const BALLS_MAIN_MAX = 49;

    // Number of balls to draw from main set
    const DRAW_MAIN_MIN = 5;
    const DRAW_MAIN_MAX = 7;

    // Number of balls in the powerball set
    const BALLS_POWER_MIN = 5;
    const BALLS_POWER_MAX = 49;

    // Number of balls to draw from powerball set
    const DRAW_POWER_MIN = 0;
    const DRAW_POWER_MAX = 3;

    public function __construct()
    {

        /* Check configuration */

        $configErrors = '';

        if (self::LAST_DRAWS > self::SAVED_DRAWS) {
            $configErrors .= 'LAST_DRAWS must be less than or equal to SAVED_DRAWS<br>';
        }
        if (self::BALLS_MAIN_MIN > self::BALLS_MAIN_MAX) {
            $configErrors .= 'BALLS_MAIN_MIN must be less than or equal to BALLS_MAIN_MAX<br>';
        }
        if (self::DRAW_MAIN_MIN > self::DRAW_MAIN_MAX) {
            $configErrors .= 'DRAW_MAIN_MIN must be less than or equal to DRAW_MAIN_MAX<br>';
        }
        if (self::BALLS_POWER_MIN > self::BALLS_POWER_MAX) {
            $configErrors .= 'BALLS_POWER_MIN must be less than or equal to BALLS_POWER_MAX<br>';
        }
        if (self::DRAW_POWER_MIN > self::DRAW_POWER_MAX) {
            $configErrors .= 'DRAW_POWER_MIN must be less than or equal to DRAW_POWER_MAX<br>';
        }

        $tempdir = sys_get_temp_dir();
        if ($tempdir == '') {
            $configErrors .= 'This system has no temporary directory configured<br>';
        } else {
            if (is_writable($tempdir)) {
                $this->file = $tempdir.'/lottodraws.csv';
            } else {
                $configErrors .= $tempdir . 'is not writeable by the webserver user<br>';
            }

        }

        if ($configErrors != '') die('Configuration errors:<br>' . $configErrors);

    }

    public function draw()
    {

        /* Return an array of all drawn balls */

        $aPicks = array();

        if (version_compare(PHP_VERSION, '7.0.0') >= 0) {

            /* Use PHP 7's improved randomness */

            // Pick the number of main balls in the set and to be drawn (subset)
            $numBallsMain = random_int(self::BALLS_MAIN_MIN, self::BALLS_MAIN_MAX);
            $numDrawMain = random_int(self::DRAW_MAIN_MIN, self::DRAW_MAIN_MAX);

            // Pick the number of main balls in the set and to be drawn (subset)
            $numBallsPower = random_int(self::BALLS_POWER_MIN, self::BALLS_POWER_MAX);
            $numDrawPower = random_int(self::DRAW_POWER_MIN, self::DRAW_POWER_MAX);

        } else {

            // Pick the number of main balls in the set and to be drawn (subset)
            $numBallsMain = mt_rand(self::BALLS_MAIN_MIN, self::BALLS_MAIN_MAX);
            $numDrawMain = mt_rand(self::DRAW_MAIN_MIN, self::DRAW_MAIN_MAX);

            // Pick the number of main balls in the set and to be drawn (subset)
            $numBallsPower = mt_rand(self::BALLS_POWER_MIN, self::BALLS_POWER_MAX);
            $numDrawPower = mt_rand(self::DRAW_POWER_MIN, self::DRAW_POWER_MAX);

        }

        // Create both sets of balls
        $aMain = range(1, $numBallsMain);
        $aPower = range(1, $numBallsPower);

        // Randomize both sets of balls
        shuffle($aMain);
        shuffle($aPower);

        // Pick correct numbers of winners
        $aPicksMain = array_slice($aMain, 0, $numDrawMain);
        $aPicksPower = array_slice($aPower, 0, $numDrawPower);

        // Serialize picks
        $aPicks['time'] = date('Y-m-d H:i:s');
        $aPicks['main'] = implode('-', $aPicksMain);
        $aPicks['power'] = implode('-', $aPicksPower);

        if (!$this->saveDraw($aPicks)) {
            $aPicks['error'] = 'Fatal error: Failed to save results.';
        }

        return $aPicks;

    }

    private function saveDraw($aPicks)
    {
        /* Save a new draw to CSV file
        *  Reads the whole file into memory first, fine for small files
        */

        // Read saved draws
        if (file_exists($this->file)) {
            $aSavedDraws = file($this->file);
            // Remove last draw when saved draws limit is reached
            if (count($aSavedDraws) == self::SAVED_DRAWS) {
                array_pop($aSavedDraws);
            }
        } else {
            $aSavedDraws = array();
        }

        // Prepend new draw
        $newDraw = implode(',', $aPicks) . PHP_EOL;
        array_unshift($aSavedDraws, $newDraw);

        // Save all
        if (@file_put_contents($this->file, $aSavedDraws)) {
            return true;
        } else {
            return false;
        }
    }

    public function getDraws($limit)
    {
        /* Read saved draws from CSV file, up to $limit lines */

        $handle = fopen($this->file, 'r');
        if ($handle) {
            $lines = 0;
            while (($line = fgets($handle)) !== false) {
                $aSavedDraws[] = $line;
                $lines++;
                if ($lines == self::LAST_DRAWS) break;
            }
            fclose($handle);
            return $aSavedDraws;
        } else {
            return false;
        } 
    }

    public function export()
    {
        /* Export CSV file */

        if (file_exists($this->file)) {
            header('Content-Type: text/csv; charset=utf-8');
            header('Content-Disposition: attachment; filename=lottodraws.csv');

            echo 'Time,Main Set,Power Set' . PHP_EOL;
            $output = fopen('php://output', 'w');
            readfile($this->file, false, $output);
            die();
        } else {
            echo "Export error: $this->file file not found!<br>";
        }
    }

}

?>
